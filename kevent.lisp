;; The MIT License

;; Copyright (c) 2009-2010, MMER Foundation. All right reserved.

;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
;; THE SOFTWARE.

(in-package :eve)

(defexport +evfilt-read+	-1)
(defexport +evfilt-write+	-2)
(defexport +evfilt-aio+		-3)  ; attached to aio requests
(defexport +evfilt-vnode+	-4)  ; attached to vnodes
(defexport +evfilt-proc+	-5)  ; attached to struct proc
(defexport +evfilt-signal+	-6) ; attached to struct proc
(defexport +evfilt-timer+	-7) ; timers
;; (defexport evfilt-netdev	-8) ; network devices /* no longer supported */
(defexport +evfilt-fs+		-9) ; filesystem events
(defexport +evfilt-lio+		-10) ; attached to lio requests
(defexport +evfilt-user+	-11)
(defexport +evfilt-syscount+	 10)

;; ACTIONS
(defexport +ev-add+		#x0001) ;;  add event to kq (implies enable)
(defexport +ev-delete+		#x0002) ;;  delete event from kq
(defexport +ev-enable+		#x0004) ;;  enable event
(defexport +ev-disable+		#x0008) ;;  disable event (not reported)

;; FLAGS
(defexport +ev-oneshot+	#x0010) ;;  only report one occurrence
(defexport +ev-clear+		#x0020) ;;  clear event state after reporting

(defexport +ev-sysflags+	#xF000) ;;  reserved by system
(defexport +ev-flag1+		#x2000) ;;  filter-specific flag

;; returned values
(defexport +ev-eof+    #x8000) ;;  EOF detected
(defexport +ev-error+  #x4000) ;;  error, data contains errno

;; data/hint flags/masks for EVFILT_USER, shared with userspace
;;
;; On input, the top two bits of fflags specifies how the lower twenty four
;; bits should be applied to the stored value of fflags.
;;
;; On output, the top two bits will always be set to NOTE_FFNOP and the
;; remaining twenty four bits will contain the stored fflags value.
(defexport +note-ffnop+	     #x00000000) ;; ignore input fflags
(defexport +note-ffand+      #x40000000) ;; AND fflags
(defexport +note-ffor+       #x80000000) ;; OR fflags
(defexport +note-ffcopy+     #xc0000000) ;; copy fflags
(defexport +note-ffctrlmask+ #xc0000000) ;; masks for operations
(defexport +note-fflagsmask+ #x00ffffff) ;;
(defexport +note-trigger+    #x01000000) ;; Cause the event to triggered for output.

;; data/hint flags for EVFILT-{READ|WRITE}, shared with userspace
(defexport +note-lowat+   #x0001) ;;  low water mark

;; data/hint flags for EVFILT-VNODE, shared with userspace
(defexport +note-delete+ #x0001) ;; vnode was removed
(defexport +note-write+  #x0002) ;; data contents changed
(defexport +note-extend+ #x0004) ;; size increased
(defexport +note-attrib+ #x0008) ;; attributes changed
(defexport +note-link+   #x0010) ;; link count changed
(defexport +note-rename+ #x0020) ;; vnode was renamed
(defexport +note-revoke+ #x0040) ;; vnode access was revoked

;; data/hint flags for EVFILT-PROC, shared with userspace
(defexport +note-exit+   #x80000000) ;; process exited
(defexport +note-fork+   #x40000000) ;; process forked
(defexport +note-exec+   #x20000000) ;; process exec'd
(defexport +note-pctrlmask+ #xf0000000) ;; mask for hint bits
(defexport +note-pdatamask+ #x000fffff) ;; mask for pid

;; additional flags for EVFILT-PROC
(defexport +note-track+	#x00000001) ;;  follow across forks
(defexport +note-trackerr+	#x00000002) ;;  could not track child
(defexport +note-child+	#x00000004) ;;  am a child process

;; You basicly have to treat a directory as a normal file..  if a NOTE_WRITE
;; or NOTE_EXTEND gets returned on a directory, you have to rescan the
;; entire directory to see what changed...  kqueue does not return the
;; directory entry in a watched directory that change/added/removed..

(define-foreign-library libc
  (t (:default "libc")))

(use-foreign-library libc)

(defcstruct ccx-kevent
  "The kevent structure"
  (ccx-ident (:pointer :uint))
  (ccx-filter :short)
  (ccx-flags :ushort)
  (ccx-fflags :uint)
  (ccx-data (:pointer :int))
  (ccx-udata :pointer ))

(defparameter udata-table (make-hash-table :test 'equalp))

(let ((kqueue nil))
  (defun kevent-init ()
    (when  (null kqueue)
      (setf kqueue (ccx-kqueue) ))
    kqueue)

  (defun kevent-dest ()
    (handler-case
	(if (zerop (ccx-close kqueue))
	    (progn
	      (setf kqueue nil) t)
	    nil)
      (type-error ()))))

(defun :ident (ident)
  (typecase ident
    (integer (make-pointer ident))
    (sb-sys:fd-stream (make-pointer (sb-sys:fd-stream-fd ident)))))

(defun ev-set (kqueue ident filter flags fflags data udata)
  (with-foreign-objects ((changelist 'ccx-kevent))
    (with-foreign-slots ((ccx-ident ccx-filter ccx-flags ccx-fflags ccx-data ccx-udata) changelist ccx-kevent)
      (setf ccx-ident  ident)		;uintptr_t ident
      (setf ccx-filter filter)		;short     filter
      (setf ccx-flags  flags)		;u_short   flags
      (setf ccx-fflags fflags)		;u_int     fflags
      (setf ccx-data (make-pointer data))	;intptr_t  data
      (setf ccx-udata udata) 		;void      *udata
      (ccx-kevent kqueue changelist 1 (null-pointer) 0 (null-pointer)))))

(defun kevent-read (kqueue &optional &key (timeout nil timeout-p))
  (with-foreign-objects  ((eventlist 'ccx-kevent 1024)
			  (timespec 'ccx-timespec))
    ;; Need to wait for getting results into eventlist structure.
    (let ((result (ccx-kevent kqueue (null-pointer) 0 eventlist 1024
			      (if timeout-p (set-timeout timespec timeout 0) (null-pointer)))))
      (loop for index from 0 below result collect
	 ;; Each rotation allow us to read the data, after that we
	 ;; need to move through the data to  another portion of it.
	   (with-foreign-slots ((ccx-ident ccx-filter ccx-flags ccx-fflags ccx-data ccx-udata)
				(mem-aref eventlist 'ccx-kevent index) ccx-kevent)
	     (list ccx-filter ccx-flags ccx-fflags (sb-sys:sap-int ccx-data)
		   (gethash (convert-from-foreign ccx-udata :string) udata-table)))))))

(defun ccx-kqueue ()
  "Creates a new kernel event queue and returns a descriptor."
  (ccx-foreign-funcall "kqueue" :int))

(defun ccx-kevent (fd changelist nchanges eventlist nevents timeout)
  "The kevent() system call is used to register events with the queue,
   and return any pending events to the user."
  (ccx-foreign-funcall "kevent" :int fd :pointer changelist
		       :int nchanges :pointer eventlist :int nevents :pointer timeout :int))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CLOS

(defclass kevent (libevent) ())

(defmethod libevent-load ((class kevent) &optional &rest options)
  (setf (libevent-ident class) (kevent-init)))

(defmethod libevent-unload ((class kevent))
  (setf (libevent-ident class) (kevent-dest)))

(defmethod event-read ((class kevent) &optional &key timeout)
  (kevent-read (kevent-init)))

(defmethod event-set-event ((class kevent) &rest args)
  (let ((udata-key (princ-to-string (gensym "kevent"))))
    (destructuring-bind (fd type action event data udata) args
      (setf (gethash udata-key udata-table) udata)
      (ev-set (libevent-ident class)
	      (:ident fd)
	      type
	      action
	      event
	      (if (null data) 0 data)
	      (convert-to-foreign udata-key :string)))))

(defmethod event-set ((class kevent) (object sb-sys:fd-stream) (event (eql :on-write))
		      &optional udata)
  (event-set-event class object +evfilt-vnode+
		   (logior +ev-add+ +ev-enable+ +ev-clear+) +note-write+ nil udata))

(defmethod event-set  ((class kevent) (object sb-sys:fd-stream) (event (eql :on-rename))
		       &optional udata)
  (event-set-event class object +evfilt-vnode+
		   (logior +ev-add+ +ev-enable+ +ev-clear+) +note-rename+ nil udata))

(defmethod event-set  ((class kevent) (object sb-sys:fd-stream) (event (eql :on-delete))
		       &optional udata)
  (event-set-event class object +evfilt-vnode+
		   (logior +ev-add+ +ev-enable+ +ev-clear+) +note-delete++ nil udata))

