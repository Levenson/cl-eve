;; The MIT License

;; Copyright (c) 2009-2010, MMER Foundation. All right reserved.

;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
;; THE SOFTWARE.

(in-package :eve)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *fd-setsize* 1024)

  (defvar *fd-bits-size* (/ *fd-setsize* sb-vm:n-machine-word-bits))

  (defcvar "errno" :int))

(defcstruct ccx-timespec
  "Timeout structure"
  (ccx-tv-sec (:int))
  (ccx-tv-nsec (:long)))

(defcstruct ccx-fd-set
  (fds-bits :long :count #.*fd-bits-size*))

(defmacro defexport (name value &optional documentation)
  "Define & export constant variable"
  `(progn
     (defconstant ,name ,value ,documentation)
     (export ',name)))

(defmacro ccx-foreign-funcall (name-and-options &rest args)
  "Return a FUNCALL result, and report an error if it."
  (with-gensyms (result)
	`(progn
	   (setf *errno* 0)
	   (let ((,result ,`(foreign-funcall ,name-and-options ,@args) ))
		 (cond
		   ((and (realp ,result)
				 (minusp ,result))
			(warn "Function:~a; errno:~d  ~a" ,name-and-options ,*errno* ,(foreign-funcall "strerror" :int *errno* :string))))
		 ,result))))

(defun ccx-close (fd)
  (ccx-foreign-funcall "close" :int fd :int))

(defun ccx-select (nfds readfds writefds exceptfds timeout)
  (ccx-foreign-funcall "select" :int nfds :pointer readfds :pointer writefds
		       :pointer exceptfds :pointer timeout :int))

(defun ccx-pselect (nfds readfds writefds exceptfds timeout sigmask)
  (ccx-foreign-funcall "pselect" :int nfds :pointer readfds :pointer writefds
		       :pointer exceptfds :pointer timeout :pointer sigmask :int))

(defmacro fd-set (offset fd-set)
  (with-gensyms (word bit)
    `(with-foreign-slots ((fds-bits) ,fd-set ccx-fd-set)
       (multiple-value-bind (,word ,bit) (floor ,offset *fd-bits-size*)
	 (setf (mem-aref fds-bits :long ,word)
	       (logior (the (unsigned-byte #.*fd-bits-size*)
			 (ash 1 ,bit))
		       (mem-aref fds-bits :long ,word)))))))


(defmacro fd-clr (offset fd-set)
  (with-gensyms (word bit)
    `(with-foreign-slots ((fds-bits) ,fd-set ccx-fd-set)
       (multiple-value-bind (,word ,bit) (floor ,offset sb-vm:n-machine-word-bits)
	 (setf (mem-aref fds-bits :long ,word)
	       (logand (the (unsigned-byte #.*fd-bits-size*)
			 (ash 1 ,bit))
		       (mem-ref fds-bits :long ,word)))))))

(defmacro fd-isset (offset fd-set)
  (with-gensyms (word bit)
    `(with-foreign-slots ((fds-bits) ,fd-set ccx-fd-set)
       (multiple-value-bind (,word ,bit) (floor ,offset *fd-bits-size*)
	 (logbitp ,bit (mem-aref fds-bits :long ,word))))))

(defmacro fd-zero (&rest fd-sets)
  `(progn
     ,@(loop for el in fd-sets
	  collect `(with-foreign-slots ((fds-bits) ,el ccx-fd-set)
		     ,@(loop for index upfrom 0 below *fd-bits-size*
			  collect `(setf (mem-aref fds-bits :long ,index) 0))))))

(defmacro fd-set-print (&rest fd-sets)
  "Print fd-set in a human readable way."
  `(progn
     ,@(loop for el in fd-sets
	  collect `(progn
		     (format t "~&~s: " ',el)
		     ,@(loop for index upfrom 0 below *fd-bits-size* collect
		     	    `(format t "~d " (mem-aref (foreign-slot-value ,el 'ccx-fd-set 'fds-bits) :long ,index))
		     	  finally (format t "~%"))))))

(defmacro with-select ((fd &optional (timeout nil)) &body body)
  (let ((obj (list `(,fd 'ccx-fd-set))))
    (when (not (null timeout))
      (push `(,timeout 'ccx-timespec) obj))
    `(with-foreign-objects (,@obj)
       ,@body)))

(defmacro set-timeout (timespec sec &optional (nsec 0))
  `(with-foreign-slots ((ccx-tv-sec ccx-tv-nsec) ,timespec ccx-timespec)
     (setf ccx-tv-sec ,sec ccx-tv-nsec ,nsec) ,timespec))

(defun tall-list (lst)
  (loop for element in lst
     if (atom element) collect element
     else append (tall-list element)))
