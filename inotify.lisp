;; The MIT License

;; Copyright (c) 2009-2010, MMER Foundation. All right reserved.

;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
;; THE SOFTWARE.

(in-package :eve)

;; Flags for the parameter of inotify_init1.
(defexport +o-nonblock+	 #x00000800)
(defexport +o-cloexec+	 #x00080000)

(defexport +in-nonblock+ +o-nonblock+)
(defexport +in-cloexec+	 +o-cloexec+)

;; Supported events suitable for MASK parameter of INOTIFY_ADD_WATCH.  
(defexport +in-access+	 #x00000001 "File was accessed.")
(defexport +in-modify+	 #x00000002 "File was modified.")
(defexport +in-attrib+	 #x00000004 "Metadata changed.")
(defexport +in-close-write+ #x00000008 "Writtable file was closed.")
(defexport +in-close-nowrite+  #x00000010 "Unwrittable file closed.")
(defexport +in-close+	 (logior +in-close-write+ +in-close-nowrite+) "Close.")
(defexport +in-open+	 #x00000020 "File was opened.")
(defexport +in-moved-from+	 #x00000040 "File was moved from X.")
(defexport +in-moved-to+	 #x00000080 "File was moved to Y.")
(defexport +in-move+	 (logior +in-moved-from+ +in-moved-to+) "Moves.")    
(defexport +in-create+	 #x00000100 "Subfile was created.")
(defexport +in-delete+	 #x00000200 "Subfile was deleted.")
(defexport +in-delete-self+ #x00000400 "Self was deleted.")
(defexport +in-move-self+	 #x00000800 "Self was moved.")

;; Events sent by the kernel.  
(defexport +in-unmount+	 #x00002000 "Backing fs was unmounted.")
(defexport +in-q-overflow+	 #x00004000 "Event queued overflowed.")
(defexport +in-ignored+	 #x00008000 "File was ignored.")

;; Helper events.
(defexport +in-close+	 (logior +in-close-write+ +in-close-nowrite+) "Close.")
(defexport +in-move+	 (logior +in-moved-from+ +in-moved-to+) "Moves.")

;; Special flags.
(defexport +in-onlydir+	 #x01000000 "Only watch the path if it is a directory")
(defexport +in-dont-follow+ #x02000000 "Do not follow a sym link.")
(defexport +in-mask-add+	 #x20000000 "Add to the mask of an already watch.")
(defexport +in-isdir+	 #x40000000 "Event occurred against dir.")
(defexport +in-oneshot+	 #x80000000 "Only send event once.")

;; All events which a program can wait on.  
(defexport +in-all-events+
  (logior +in-access+  +in-modify+  +in-attrib+  +in-close-write+  
	  +in-close-nowrite+  +in-open+ +in-moved-from+	      
	  +in-moved-to+  +in-create+  +in-delete+		      
	  +in-delete-self+  +in-move-self+))

(defcstruct ccx-inotify-event
  (wd :int)
  (mask   :uint32)
  (cookie :uint32)
  (len    :uint32)
  (name  :pointer))

;; Singletone 
(let ((inotify nil))
  (defun inotify-init (&optional (flag 0))
    (when (null inotify)
      (setf inotify (ccx-inotify-init flag)))
    inotify)

  (defun inotify-dest ()
    (handler-case
	(values nil
		(if (zerop (ccx-close inotify))
		    (progn
		      (setf inotify nil) t)
		    nil))
      (type-error ()))))

(defun inotify-set (inotify event-object mask)
  (if (null inotify)
      (error "Need to initializes first.")
      ;; INOTIFY_ADD_WATCH waits for full path-name to the object.
      (ccx-inotify-add-watch inotify (namestring (pathname event-object)) mask)))

(defun inotify-read-buffer-events (inotify)
  "Reads block of data from inotify file descriptor"
  (with-foreign-object (buffer 'ccx-inotify-event 1024)
    (let ((res (ccx-read inotify buffer 1024)))
      (if (not (minusp res))
	  ;; Need to subtract 4 bytes from the full structure size - it's name filed size. We need to read it
	  ;; (name), if sizeof(len) > 0. It will happend when our object is directory, and the name field will
	  ;; collect the file-name on wich actions were done
	  (loop with offset = 0 and size-of-current-event = (- (foreign-type-size 'ccx-inotify-event) 4)
	     while (< offset res ) collecting
	       (progn
		 (let ((offset-buff (mem-ref buffer 'ccx-inotify-event offset)) (file-name ""))
		   (with-foreign-slots ((wd mask cookie len name) offset-buff  ccx-inotify-event)
		     (if (plusp len)
			 (progn
			   (setf offset (+ offset size-of-current-event len))
			   (setf file-name (foreign-string-to-lisp (foreign-slot-pointer offset-buff 'ccx-inotify-event 'name))))
			 (setf offset (+ offset size-of-current-event)))
		     ;; (format t "~&wd: ~d  mask: 0x~8,'0,0,2X cookie:~d offset: ~d len:~d name ~@S"
		     ;; 	      wd mask cookie offset len file-name)
		     (list wd mask cookie offset len file-name)))))
	  res))))

(defun inotify-read (inotify &optional &key (timeout nil timeout-p))
  (if (and timeout-p 
	   (not (null timeout)))
      (with-select (fd-set timespec)
	(set-timeout timespec timeout 0)
	(fd-zero fd-set)
	(fd-set inotify fd-set)
	(if (zerop (ccx-select *fd-setsize* fd-set (null-pointer) (null-pointer) timespec))
	    ;; Already done in CCX-FUNCALL
	    ;; ((minusp result) (warn "select() error. look at the errno"))
	    (warn "Timespec expired.")	 
	    (when (fd-isset inotify fd-set) 
	      (inotify-read-buffer-events inotify))))
      (inotify-read-buffer-events inotify)))

(defun ccx-read (fd buf count)
  (ccx-foreign-funcall "read" :int fd :pointer buf  :int count :int))

(defun ccx-inotify-init (&optional (flag 0))
  "initializes a new inotify instance and returns
   a file descriptor associated with a new inotify event queue."
  (ccx-foreign-funcall "inotify_init1" :int flag :int))

(defun ccx-inotify-add-watch (fd pathname mask)
  "Add a watch to an initialized inotify instance"
  (ccx-foreign-funcall "inotify_add_watch" :int fd :string (convert-to-foreign pathname :string) :uint32 mask :int))

(defun ccx-inotify-rm-watch (fd wd)
  "Remove an existing watch from an inotify instance"
  (ccx-foreign-funcall "inotify_rm_watch" :int fd :int wd :int))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CLOS 
(defclass inotify (libevent) ())

(defmethod libevent-load ((class inotify) &rest initargs)
  (let ((flag (pop initargs)))
    (setf (libevent-ident class) (inotify-init (if flag flag 0)))))

(defmethod libevent-unload ((class inotify))
  (inotify-dest))

(defmethod libevent-flush ((class inotify))
  (loop for element in (libevent-event-table class) do
       (ccx-inotify-rm-watch (libevent-ident class) element)))

(defmethod event-read ((class inotify) &optional &key (timeout nil))
  (inotify-read (libevent-ident class) :timeout timeout))

(defmethod event-set-event ((class inotify) &rest args)
  (destructuring-bind (path event) args
    (push (inotify-set (libevent-ident class) path event)
	  (libevent-event-table class))))

(defmethod event-set ((class inotify) (object pathname) (event cons))
  (dolist (one event)
    (event-set class object one)))

(defmethod event-set ((class inotify) (object pathname) (event (eql :on-rename)))
  (event-set-event class (directory-namestring (pathname object))
		   (logior +in-mask-add+ +in-move+)))

(defmethod event-set ((class inotify) (object pathname) (event (eql :on-delete)))
  (event-set-event class (directory-namestring (pathname object))
		   (logior +in-mask-add+ +in-delete+)))

(defmethod event-set ((class inotify) (object pathname) (event (eql :on-write)))
  (event-set-event class (namestring (pathname object))
		   (logior +in-mask-add+ +in-close-write+)))

(defmethod event-set ((class inotify) (object pathname) (event (eql :on-read)))
  (event-set-event class (namestring (pathname object))
		   (logior +in-mask-add+ +in-close-nowrite+)))


