;; The MIT License

;; Copyright (c) 2009-2010, MMER Foundation. All right reserved.

;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
;; THE SOFTWARE.

(in-package :eve)

(defvar libevent-table ())

(defclass libevent ()
  ((ident :initform nil :accessor libevent-ident)
   (o-nonblock  :initform nil :initarg :o-nonblock)
   (event-table :initform nil :accessor libevent-event-table)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Libevent class operations
(defgeneric libevent-load (class &rest initargs &key &allow-other-keys)
  (:documentation "Loads associated C-library with the CLASS class."))

(defmethod libevent-load :after ((class libevent) &rest initargs)
  "Sets some flags to the opened file-descriptor of the C-library and
   push (register) it to the table"
  (when (slot-value class 'o-nonblock)
    (if (equalp (type-of class) 'kevent)
	(warn "Nonblock is inappropriate for kqueue. errno = 25")
	(let ((curr-fl (sb-posix:fcntl (slot-value class 'ident) sb-posix:f-getfl)))
	  (sb-posix:fcntl (slot-value class 'ident) sb-posix:f-setfl (logand curr-fl sb-posix:o-nonblock))))
    )
  (pushnew (list (libevent-ident class) class) libevent-table))

(defgeneric libevent-flush (class)
  (:documentation "Remove all current set events."))

(defgeneric libevent-unload (class))

(defmethod libevent-unload :after ((class libevent))
  "Remove record (unregistered) from the table of the
  pais (C-library-fd class-instance)"
  (setf libevent-table (remove-if (lambda (pair)
				    (equalp class (second pair))) libevent-table)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Events operations
(defgeneric event-read (class &optional &key timeout))

(defgeneric event-set-event (class &rest args)
  (:documentation "Interface to native library."))

(defgeneric event-unset-event (class event-class &rest args)
  (:documentation "Delete specific event from the library, if it
  implemented."))

;; Macros
(defmacro with-event ((event &rest rest) &body body)
  `(let ((,event
	  (make-instance
	   #+bsd 'kevent
	   #+linux 'inotify
	   ,@rest)))
     (unwind-protect
	  (progn
	    (libevent-load ,event)
	    ,@body)
       (libevent-unload ,event))))
